import java.util.*;

public class Process{
	private String name;
	private int pID, np, memory;
	boolean complete = false;

	public Process(String name, int pID){
		this.name = name;
		this.pID = pID;
		nProcessAssign((int)(name.charAt(0))/6);
	}

	public String getName(){
		return this.name;
	}

	public int getID(){
		return this.pID;
	}

	public int getNPA(){
		return this.np;
	}

	public void setMemory(int memory){
		this.memory = memory;
	}

	public int getMemory(){
		return this.memory;
	}

	private void nProcessAssign(int n){
		Random np = new Random();
		this.np = (np.nextInt(n)+10);
	}

	private void delay(){
		try{
			Thread.sleep(5);
		}
		catch(InterruptedException e){System.out.println("Algo salió terriblemente mal. Consulta el método \"delay\"");}
	}

	public void doProcess(){
		Random rn = new Random();

		System.out.print("\n");
		
		for(int i = 1; i <= 500 ; i++){
			if(np == 0){
				this.complete = true;
				break;
			}

			System.out.print("\\");
			delay();
			if(i == 100 || i == 200 || i == 300 || i == 400 || i == 500){
				np -= 1;
				System.out.print("\n");
			}
		}
		
	}
}