import java.util.*;

public class Principal{
	public static Scanner sc = new Scanner(System.in);

	public static int id(String name){
		int id = 10000000;

		for(int i = 0; i < name.length(); i++){
			id += (int)(name.charAt(i))*1024;
		}

		return id;
	}

	public static void pEnter(){
		System.out.println("Presione cualquier tecla para continuar.");
		sc.nextLine();
	}

	public static String jtext(String name){	//Justifica el texto del apartado 3
		int size = name.length();
		String space = " ";
		if(size > 25){
			name = name.substring(0,22);
			name = name.concat("...");
		}
		else{
			for(int i = 0 ; i < (25 - size) ; i++){
				name = space.concat(name);
			}
		}

		return name;
	}

	public static void main(String[] args){	
		ArrayList<Process> kiwi = new ArrayList<Process>();	//queue
		ArrayList<String> ekiwi = new ArrayList<String>();	//Procesos exitosos
		ArrayList<String> fkiwi = new ArrayList<String>();	//Procesos eliminados

		int actualMemory = 2048;
		String op = "";
		boolean end = true;

		do{
			System.out.println("\n\nMemoria disponible: " + actualMemory + "\n");
			System.out.print("Elija una opcion:\n 1: Crear un proceso.\n 2: Ver estado actual del sistema.\n 3: Imprimir cola de procesos.\n 4: Ejecutar proceso actual.\n 5: Ver proceso actual.\n 6: Pasar al proceso siguiente.\n 7: Matar proceso actual.\n 8: Matar todo y terminar\n 9: Salir.\n   -> ");
			op = sc.nextLine();

			switch(op){
				case "1":
					String name;
					int pID;

					System.out.print("Crear un proceso.\n");

					if(actualMemory >= 32){								//Verifica que la memoria sea la mínima necesaria para crear un proceso
						do{
							System.out.print("Escriba el nombre del proceso: ");
							name = sc.nextLine();

							if((int)(name.charAt(0)) > 126){	//Revisa si el nombre introducido es válido
								System.out.print("No puede utilizar ese nombre. Pruebe con otro.\n\n");
								name = "";
								continue;
							}

							for(Process n : kiwi){
								if(n.getName().equals(name)){	//Revisa si el nombre introducido es válido
									System.out.print("No puede utilizar ese nombre. Pruebe con otro.\n\n");
									name = "";
									break;
								}
							}
						} while(name.equals(""));

						pID = id(name);

						Process p = new Process(name, pID);				//Crea el objeto Process

						if(p.getNPA() >= 26 && actualMemory >= 512)		//Asigna la memoria a utilizar
							p.setMemory(512);

						else if(p.getNPA() >= 22 && actualMemory >= 256)
							p.setMemory(256);

						else if(p.getNPA() >= 18 && actualMemory >= 128)
							p.setMemory(128);

						else if(p.getNPA() >= 14 && actualMemory >= 64)
							p.setMemory(64);

						else if(p.getNPA() >= 10 && actualMemory >= 32)
							p.setMemory(32);

						else
							p.setMemory(actualMemory);

						actualMemory -= p.getMemory();

						kiwi.add(p);									//Añade al proceso a la cola

					}
					else
						System.out.println("Para poder crear un nuevo proceso es necesario liberar espacio.");
						
					pEnter();
					break;

				case "2":
					System.out.println("Estado actual del sistema.\n");
					if(kiwi.size() == 0)
						System.out.println("No hay procesos en la cola.");
					
					else{
						System.out.println("Total de procesos listos: " + kiwi.size());
						
						System.out.println("\nProceso completados exitosamente: ");
						if(ekiwi.isEmpty())
							System.out.println("No se han completado procesos.");
						else
							for(String ne : ekiwi)
								System.out.println("\t" + ne);

						System.out.println("\nProceso eliminados: ");
						if(fkiwi.isEmpty())
							System.out.println("No se han detenido procesos.");
						else
							for(String nf : fkiwi)
								System.out.println("\t" + nf);

						System.out.println("\nTotal de la memoria utilizada: " + (2048-actualMemory) + "\n");	
					}

					pEnter();
					break;

				case "3":
					System.out.println("Imprimir cola de procesos.\n");
					System.out.println("||     Nombre del proceso        || ID ||   Instrucciones || Memoria utilizada");
					for(Process cp : kiwi){
						System.out.println("   " + jtext(cp.getName()) + "     " + cp.getID() + "        " + cp.getNPA() + "		   " + cp.getMemory());
					}
					System.out.print("\n");
					pEnter();
					break;

				case "4":
					System.out.println("Ejecutar proceso actual.\n");
					if(kiwi.isEmpty()){
						System.out.println("No hay procesos para ejecutar.");
					}
					else{
						kiwi.get(0).doProcess();

						if(kiwi.get(0).complete){
							ekiwi.add(kiwi.get(0).getName());
							actualMemory += kiwi.get(0).getMemory();
							kiwi.remove(0);
						}
						else{
							kiwi.add(kiwi.get(0));
							kiwi.remove(0);
						}
					}

					pEnter();
					break;

				case "5":
					System.out.println("Ver proceso actual.\n");

					if(kiwi.size() < 1)	//Excepción
						System.out.println("No existen los procesos suficientos para esto.");

					else{
						System.out.println("Nombre: " + kiwi.get(0).getName());
						System.out.println("ID: " + kiwi.get(0).getID());
						System.out.println("No. de instrucciones: " + kiwi.get(0).getNPA());
						System.out.println("Memoria utilizada: " + kiwi.get(0).getMemory());
					}
					pEnter();
					break;

				case "6":
					if(kiwi.size() <= 1)	//Excepción
						System.out.println("No existen los procesos suficientes para esto.");
					
					else {
						System.out.println("Cambiando al proceso siguiente...\n");
						kiwi.add(kiwi.get(0));
						kiwi.remove(0);
					}

					pEnter();
					break;

				case "7":
					if(kiwi.size() < 1)	//Excepción
						System.out.println("No existen los procesos suficientes para esto.");

					else{
						System.out.print("Proceso asesinado. ");
						actualMemory += kiwi.get(0).getMemory();
						kiwi.remove(0);
					}
					
					pEnter();
					break;

				case "8":
					if(kiwi.isEmpty())
						System.out.println("No hay procesos para eliminar.");

					else{
						System.out.println("Matando todo y terminando...\n");
						for(Process queue : kiwi){
							actualMemory += queue.getMemory();
							fkiwi.add(queue.getName());
						}
						kiwi.clear();
						end = false;
					}
					
					pEnter();
					break;

				case "9":
					end = false;
					System.out.println("\n");
					break;

				default:
					System.out.print("Proceso no reconocido. ");
					pEnter();
					continue;
			}

		} while(end);
	}
}